BUILDER ?= podman
BUILD_OPTS ?= --layers --rm=false --force-rm=false --pull=missing
BUILD_PLATFORM ?= linux/amd64,linux/arm64/v8
REPO_NAME ?= localhost:5000/rdnxk/android-sdk
ASDK_REPO ?= $(REPO_NAME)

## Use 0 or false to disable. Enabled otherwise
## If you are using a local non-HTTPS registry, set INSECURE_REGISTRY to 1 or true
INSECURE_REGISTRY ?= false

PUSH_OPTS ?=
ARTIFACT_OPTS ?=

## This will push the image right after building each image
## If disabled (default behavior) it will require to call `make push` target to push images
## Use 1 or true to enable.
AUTO_PUSH ?= false

COSIGN_PRIVATE_KEY ?= cosign.key
COSIGN_PUBLIC_KEY ?= cosign.pub
COSIGN_PASSWORD ?= foobar

## BUILDING OPTIONS
### preferably, pass them as KEY1=VAL1 KEY2=VAL2 make build
# ANDROID_API_VERSION ?=
### one of the template variants
# BUILD_VARIANT ?=
### Prefix for the image, like debian, alpine
# TAG_PREFIX ?= $(BUILD_VARIANT)
# TAG_PREFIX_ALIASES ?=
