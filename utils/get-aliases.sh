#!/usr/bin/bash

## alpine, bullseye, ubuntu etc
prefix="$1"
## cmdline, tools, api-32, api-33 etc
suffix="$2"
## optional
repo="$3"

[[ -z "$prefix" ]] && echo "prefix is required" && exit 1
[[ -z "$suffix" ]] && echo "suffix is required" && exit 1

[[ "$prefix" == "$ALIAS_PREFIX_DEFAULT" ]] && aliases+=("$suffix")

prefix_aliases_array=()
aliases=()
for prefix_alias in ${TAG_PREFIX_ALIASES//,/ } ${EXTRA_PREFIX_ALIASES//,/ }; do
    prefix_aliases_array+=("$prefix_alias")
    aliases+=("$prefix_alias-$suffix")
done

function helper_fn() {
    matcher="$1"
    name="$2"
    addition="${name:+-$name}"
    if [[ "$suffix" == "$matcher" ]]; then
        for t in $prefix "${prefix_aliases_array[@]}"; do
            aliases+=("${t}${addition}")
        done
    fi
}

helper_fn "$ALIAS_SUFFIX_DEFAULT"
helper_fn "$ALIAS_SUFFIX_LATEST" "latest"
helper_fn "$ALIAS_SUFFIX_NEXT" "next"
helper_fn "$ALIAS_SUFFIX_STABLE" "stable"

if [[ "$prefix" == "$ALIAS_PREFIX_DEFAULT" ]]; then
    [[ "$suffix" == "$ALIAS_SUFFIX_LATEST" ]] && aliases+=("latest")
    [[ "$suffix" == "$ALIAS_SUFFIX_NEXT" ]] && aliases+=("next")
    [[ "$suffix" == "$ALIAS_SUFFIX_STABLE" ]] && aliases+=("stable")
fi

## add repo name if passed
if [[ -n "$repo" ]]; then
    for i in "${!aliases[@]}"; do
        val="${repo}:${aliases[$i]}"
        aliases[$i]="$val"
    done
fi

echo "${aliases[*]}"
