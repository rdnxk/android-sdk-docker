{
    ($input_file): {
        "org.opencontainers.image.title": $output_file,
        "org.opencontainers.image.created": $date,
        "org.opencontainers.image.description": $file_description
    },
    "$config": {
        "org.opencontainers.image.created": $date
    },
    "$manifest": {
        "org.opencontainers.image.title": "ReDemoNBR's Android SDK",
        "org.opencontainers.image.description": "OCI Container Image for building Android applications",
        "org.opencontainers.image.vendor": "rdnxk",
        "org.opencontainers.image.authors": "rdnxk, San 'rdn' Mônico <san@monico.com.br>",
        "org.opencontainers.image.url": "https://hub.docker.com/r/redemonbr/android-sdk",
        "org.opencontainers.image.source": "https://gitlab.com/rdnxk/android-sdk-docker",
        "org.opencontainers.image.documentation": "https://gitlab.com/rdnxk/android-sdk-docker/-/blob/master/README.md",
        "org.opencontainers.image.created": $date
    }
}
